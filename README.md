# Problème algorithmique 1

Problème d'algorithmie

# Author

### BERGES Dimitri

# Exercice

L'objectif est de retrouver quels sont les montants contenus dans un .csv qui "match" avec les valeurs suivantes :

15 180,55 | 820 287,15 | 192 726,37

Il y a 463 valeurs et la somme de toutes les valeurs est égale à la somme des 3 valeurs ci-dessus.

GL HF