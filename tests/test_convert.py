import unittest
from own_convert.functions import convert

class Test_Own_CSV(unittest.TestCase):

    def test_convert(self):
        data_not_traited = '"1 000,5"'
        pattern = [
            {"name":"split","split":'"', "index":1},
            {"name":"replace","replace":' ', "value":''},
            {"name":"replace","replace":",", "value":'.'},
            {"name":"cast","cast":float}
        ]
        data_traited = 1000.5
        self.assertEqual(convert(data_not_traited, pattern), data_traited)