"""
    Fonction convert
    %       Cette fonction convertie une chaine de caractère passé en
            argument selon un pattern contenant des instructions à
            réaliser.
    %IN     string : Chaine de caractère à convertir,
            pattern : Dictionnaire d'instruction pour convertir la chaine
    %OUT    out : Donnée convertie
"""

def convert(string, pattern):
    out = string
    for operation in pattern:
        if operation['name'] == "split":
            out = out.split(operation["split"])[operation['index']]
        elif operation['name'] == "replace":
            table = out.split(operation["replace"])
            out = ""
            first_row = True
            for row in table:
                if first_row == False:
                    out = out + operation['value']
                out = out + row
                if first_row == True:
                    first_row = False
        elif operation['name'] == "cast":
            out = (operation['cast'])(out)
    return out