# Module own_convert

## Description

Ce module nous permet de convertir des chaines de caractères avec des instructions précises.

### Fonctions

Dans ce module on possède qu'1 fonction :

* convert() qui convertit une chaine de caractères selon un pattern.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_convert.
On y teste notre fonction avec un lot de donnée et 1 pattern précis.
