# Module own_search

## Description

Ce module nous permet de rechercher des combinaisons dans des listes.

### Fonctions

Dans ce module on possède 3 fonctions :

* search_combination_add() qui recherche toutes les combinaisons pouvant atteindre une valeur souhaitée.
* sum_list() qui nous retourne la somme de tout les éléments d'une liste.
* move_circular() qui nous renvoie une valeur comprise entre des bornes minimales et maximales en effectuant un mouvement circulaire au besoin.

### Tests

Les tests sont dans le module tests du projet au sein du fichier test_search.
On y teste nos fonctions avec plusieurs jeu de données.
